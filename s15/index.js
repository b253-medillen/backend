// Comments: 

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/*
	There are two types of comments:
	1. Single-line comment - denoted by two slashes
	2. Multi-line Comment - denoted by a forward slash and two asterisks
*/

console.log ("Hello Batch 253!");

/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors because since the HTML elements have not yet been loaded when the JavaScript loads, it does not have any valid HTML elements to target/select.
*/

/*
	Synstax - this is the grammar of a language
	Statements - this is the commands or instructions for the computer
*/

// [SECTION] Variables

// It is used to contain data
// Any information that is used by an application is stored in what we call a memory.
// When we ceate variables, certain portions of a devices memory is given a "name" that we call variables
// This makes it easier for us to associate information stored in our devices to actual "names" about information
// The names of a variable is called identifier

// Declaring variables:

// Declaring variables - tells our devices that a variable name is created and is ready to store data
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

/*
	Syntax:
	Let/const variableName
*/
// Let is a keyword that is usually usd in declaring a variable.

let feelings

/*console.log(feelings);

let hello;*/

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
Best practices in naming variables:

		1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

			let firstName = "Michael"; - good variable name
			let pokemon = 25000; - bad variable name
2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

			let FirstName = "Michael"; - bad variable name
			let firstName = "Michael"; - good variable name
3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

			let first name = "Mike";

		camelCase is when we have first word in small caps and the next word added without space but is capitalized:

			lastName emailAddress mobileNumber
Underscores sample:

		let product_description = "lorem ipsum"
		let product_id = "250000ea1000"

*/

// Initializing variables 
// Initializing variables - the instance when a variable is given it's initial/starting value
// Syntax
// let/const variableName = value;
let productName = "desktop computer";
console.log (productName);


let product_Price = "18999";
console.log (product_Price);



// Let variable cannot be re-declared within its scope.
let friend = "Owen";
friend = "KEVIN";
friend = "casper";
console.log(friend);

//while const cannot be updated or re-declared
// Values of constants cannot be changed and will simply return an error
// interest = 3.14159265; TypeError; Assignment to contact variable.
const interest = 3.539;
console.log (interest);

// This is a declaration of variable since it has no value yet
let supplier;
// This is still an initialization of variable since it is the first time assigning its value
supplier = "John Smith Tradings";
// This is considered as re-assignment because its initial value was already declared.
supplier = "Zuitt store";

// Constant cannot be declared without initialization
const pi = 3.14159265;
console.log(pi);

//There are issues associated with variables declared with var, regarding with hoisting.
//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.
//Hoisting is JavaScript's default behavior of moving declarations to the top.
var pokemon = "pikachu";
console.log (pokemon);

/*a = 5;
console.log (a);
let a;*/

/*let outerVar = "hello from the outside";

{
	let innerVar = "hello from the inside"
};

console.log (outerVar);
console.log (innerVar);*/

console.log (supplier, product_Price, interest);

//  [SECTION] Data Types


// =============STRINGS
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote
// In other programming languages, only the double quotes can be used for creating strings

let country = 'Philippines';
let city = "cebu"; 

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol
let fullAddress = city + ', ' + country;
console.log (fullAddress);

let greeting = "I live in the " + country;
console.log (greeting);


console.log ("My name is Jonathan. " + " " + greeting);

// (\) escape character - in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text
let mailAddress = "Metro Manila \n\nPhilippines";
console.log (mailAddress);

let message = "John's employees went home early today";
console.log (message);
message = 'John\'s employees went home early today';
console.log (message);
message = "John said to his employees \"Hello\"";
console.log (message)

// ========== NUMBERS
// Integer/Whole Number
let headcount = 26;
console.log (headcount);

// Decimal Number/float/fractions
let grade = 98.7;
console.log (grade);

// Euler's number
let planetDistance = 2e10;
console.log (planetDistance);

// data type number will become a string once it is combine/concatenated with a string.
console.log ("John's grade last quarter is " + grade);

// Boolean
// // Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios
let isMarried = false;
let ingGoodConduct = true;
console.log ("isMarried: " + isMarried);
console.log ("ingGoodConduct: " + ingGoodConduct);

// ============= Arrays
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types BUT is normally used to store SIMILAR data types
/*
	Styntax:
	let/const arrayName = [elementA, elementB, elementC, ...]
*/
let grades = [98.7, 92.1, 90.2, 94.6];
console.log (grades);

// different data types
// storing different data types inside an array is not recommended because it will not make sense to in the context of programming
let details = ["John", "Smith", 32, true];
console.log (details);

// ============== OBJECTS
// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object
/*
	Syntax:
		let/const objectName = {
			propertyA: value,
			propertyB: value,
			...
		}

*/
let person = {
	fullName: "Bilbo Baggins",
	age: 90,
	isMarried: false,
	contact: ["0912 3456 789"],
	address: {
		houseNumber: '123',
		city: "Shire"
	}
};
console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};
console.log(myGrades);

// ==typeof operator is used to determine the type of data or the value of a variable. Its  output is a string.
console.log(typeof myGrades);
// Note: Array is a special type of object with methods and functions to manipulate it.
console.log(typeof grades);

const anime = ["Madoka Magical", "Parasyte", "One Piece"];
// anime = ["bungou stray dogs"];
anime[0] = "kimetsu no yaiba";
console.log(anime);

// NULL
// It is used to intentionally express the absence of a value in a variable declaration/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
let spouse = null, myNumber = 0, myString = '';
console.log(spouse);
console.log(myNumber);
console.log(myString);

// ==============UNDEFINED
// Represents the states of a variable that has been declared but without an assigned value.
console.log("Feelings is " + feelings);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing

